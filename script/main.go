package main

import (
	"context"
	"faucetgo/api" // your generated smart contract bindings
	"log"
	"net/http"
	"os"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func goDotEnvVariable(key string) string {

	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

func main() {
	client, err := ethclient.Dial(goDotEnvVariable("RPC_URL"))
	if err != nil {
		panic(err)
	}

	privateKey, err := crypto.HexToECDSA(goDotEnvVariable("PRIVATE_KEY"))
	if err != nil {
		panic(err)
	}

	chainID, err := client.ChainID(context.Background())
	if err != nil {
		panic(err)
	}

	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, chainID)
	if err != nil {
		panic(err)
	}

	conn, err := api.NewApi(common.HexToAddress(goDotEnvVariable("CONTRACT_ADDRESS")), client)
	if err != nil {
		panic(err)
	}

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	type Response struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}

	// Endpoint
	e.GET("/request/:_address", func(c echo.Context) error {
		_address := c.Param("_address")
		_, err := conn.RequestTokens(auth, common.HexToAddress(_address))
		if err != nil {
			return c.JSON(http.StatusInternalServerError, err)
		}
		r := &Response{
			Status:  "OK",
			Message: "XTH transferred to your wallet!",
		}
		return c.JSON(http.StatusOK, r)
	})

	// Start server
	e.Logger.Fatal(e.Start(":1324"))
}
