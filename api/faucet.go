// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package api

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// ApiMetaData contains all meta data concerning the Api contract.
var ApiMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"stateMutability\":\"payable\",\"type\":\"constructor\"},{\"inputs\":[],\"name\":\"amountAllowed\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"donateTofaucet\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"lockTime\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"addresspayable\",\"name\":\"_requestor\",\"type\":\"address\"}],\"name\":\"requestTokens\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"newAmountAllowed\",\"type\":\"uint256\"}],\"name\":\"setAmountallowed\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"setOwner\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405267016345785d8a0000600155600080546001600160a01b031916331790556104aa806100316000396000f3fe6080604052600436106100705760003560e01c80638da5cb5b1161004e5780638da5cb5b146100aa5780639ad2cd14146100e7578063a4beda631461010b578063f3aa52a11461013857600080fd5b806313af403514610075578063580f390414610097578063605e73cd14610095575b600080fd5b34801561008157600080fd5b506100956100903660046103cf565b610158565b005b6100956100a53660046103cf565b6101ad565b3480156100b657600080fd5b506000546100ca906001600160a01b031681565b6040516001600160a01b0390911681526020015b60405180910390f35b3480156100f357600080fd5b506100fd60015481565b6040519081526020016100de565b34801561011757600080fd5b506100fd6101263660046103cf565b60026020526000908152604090205481565b34801561014457600080fd5b506100956101533660046103f3565b610388565b6000546001600160a01b0316331461018b5760405162461bcd60e51b81526004016101829061040c565b60405180910390fd5b600080546001600160a01b0319166001600160a01b0392909216919091179055565b6001600160a01b038116600090815260026020526040902054421161023a5760405162461bcd60e51b815260206004820152603960248201527f4c6f636b2074696d65202831206461792920686173206e6f742065787069726560448201527f642e20506c656173652074727920616761696e206c61746572000000000000006064820152608401610182565b60015447116102a15760405162461bcd60e51b815260206004820152602d60248201527f4e6f7420656e6f7567682066756e647320696e20746865206661756365742e2060448201526c506c6561736520646f6e61746560981b6064820152608401610182565b600154816001600160a01b031631106102fc5760405162461bcd60e51b815260206004820152601f60248201527f596f7520616c7265616479206861766520656e6f7567682062616c616e6365006044820152606401610182565b6001546040516000916001600160a01b038416918381818185875af1925050503d8060008114610348576040519150601f19603f3d011682016040523d82523d6000602084013e61034d565b606091505b505090508061035b57600080fd5b610368426201518061044e565b6001600160a01b0390921660009081526002602052604090209190915550565b6000546001600160a01b031633146103b25760405162461bcd60e51b81526004016101829061040c565b600155565b6001600160a01b03811681146103cc57600080fd5b50565b6000602082840312156103e157600080fd5b81356103ec816103b7565b9392505050565b60006020828403121561040557600080fd5b5035919050565b60208082526022908201527f4f6e6c79206f776e65722063616e2063616c6c20746869732066756e6374696f604082015261371760f11b606082015260800190565b6000821982111561046f57634e487b7160e01b600052601160045260246000fd5b50019056fea2646970667358221220dfc4496e87e4c0cfa92650c04c7c4729f698f41a4b5892ba5badc5b9682e023c64736f6c634300080f0033",
}

// ApiABI is the input ABI used to generate the binding from.
// Deprecated: Use ApiMetaData.ABI instead.
var ApiABI = ApiMetaData.ABI

// ApiBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use ApiMetaData.Bin instead.
var ApiBin = ApiMetaData.Bin

// DeployApi deploys a new Ethereum contract, binding an instance of Api to it.
func DeployApi(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Api, error) {
	parsed, err := ApiMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(ApiBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Api{ApiCaller: ApiCaller{contract: contract}, ApiTransactor: ApiTransactor{contract: contract}, ApiFilterer: ApiFilterer{contract: contract}}, nil
}

// Api is an auto generated Go binding around an Ethereum contract.
type Api struct {
	ApiCaller     // Read-only binding to the contract
	ApiTransactor // Write-only binding to the contract
	ApiFilterer   // Log filterer for contract events
}

// ApiCaller is an auto generated read-only Go binding around an Ethereum contract.
type ApiCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ApiTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ApiTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ApiFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ApiFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ApiSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ApiSession struct {
	Contract     *Api              // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ApiCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ApiCallerSession struct {
	Contract *ApiCaller    // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// ApiTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ApiTransactorSession struct {
	Contract     *ApiTransactor    // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ApiRaw is an auto generated low-level Go binding around an Ethereum contract.
type ApiRaw struct {
	Contract *Api // Generic contract binding to access the raw methods on
}

// ApiCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ApiCallerRaw struct {
	Contract *ApiCaller // Generic read-only contract binding to access the raw methods on
}

// ApiTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ApiTransactorRaw struct {
	Contract *ApiTransactor // Generic write-only contract binding to access the raw methods on
}

// NewApi creates a new instance of Api, bound to a specific deployed contract.
func NewApi(address common.Address, backend bind.ContractBackend) (*Api, error) {
	contract, err := bindApi(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Api{ApiCaller: ApiCaller{contract: contract}, ApiTransactor: ApiTransactor{contract: contract}, ApiFilterer: ApiFilterer{contract: contract}}, nil
}

// NewApiCaller creates a new read-only instance of Api, bound to a specific deployed contract.
func NewApiCaller(address common.Address, caller bind.ContractCaller) (*ApiCaller, error) {
	contract, err := bindApi(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ApiCaller{contract: contract}, nil
}

// NewApiTransactor creates a new write-only instance of Api, bound to a specific deployed contract.
func NewApiTransactor(address common.Address, transactor bind.ContractTransactor) (*ApiTransactor, error) {
	contract, err := bindApi(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ApiTransactor{contract: contract}, nil
}

// NewApiFilterer creates a new log filterer instance of Api, bound to a specific deployed contract.
func NewApiFilterer(address common.Address, filterer bind.ContractFilterer) (*ApiFilterer, error) {
	contract, err := bindApi(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ApiFilterer{contract: contract}, nil
}

// bindApi binds a generic wrapper to an already deployed contract.
func bindApi(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ApiABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Api *ApiRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Api.Contract.ApiCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Api *ApiRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Api.Contract.ApiTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Api *ApiRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Api.Contract.ApiTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Api *ApiCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Api.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Api *ApiTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Api.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Api *ApiTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Api.Contract.contract.Transact(opts, method, params...)
}

// AmountAllowed is a free data retrieval call binding the contract method 0x9ad2cd14.
//
// Solidity: function amountAllowed() view returns(uint256)
func (_Api *ApiCaller) AmountAllowed(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Api.contract.Call(opts, &out, "amountAllowed")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AmountAllowed is a free data retrieval call binding the contract method 0x9ad2cd14.
//
// Solidity: function amountAllowed() view returns(uint256)
func (_Api *ApiSession) AmountAllowed() (*big.Int, error) {
	return _Api.Contract.AmountAllowed(&_Api.CallOpts)
}

// AmountAllowed is a free data retrieval call binding the contract method 0x9ad2cd14.
//
// Solidity: function amountAllowed() view returns(uint256)
func (_Api *ApiCallerSession) AmountAllowed() (*big.Int, error) {
	return _Api.Contract.AmountAllowed(&_Api.CallOpts)
}

// LockTime is a free data retrieval call binding the contract method 0xa4beda63.
//
// Solidity: function lockTime(address ) view returns(uint256)
func (_Api *ApiCaller) LockTime(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _Api.contract.Call(opts, &out, "lockTime", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// LockTime is a free data retrieval call binding the contract method 0xa4beda63.
//
// Solidity: function lockTime(address ) view returns(uint256)
func (_Api *ApiSession) LockTime(arg0 common.Address) (*big.Int, error) {
	return _Api.Contract.LockTime(&_Api.CallOpts, arg0)
}

// LockTime is a free data retrieval call binding the contract method 0xa4beda63.
//
// Solidity: function lockTime(address ) view returns(uint256)
func (_Api *ApiCallerSession) LockTime(arg0 common.Address) (*big.Int, error) {
	return _Api.Contract.LockTime(&_Api.CallOpts, arg0)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Api *ApiCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Api.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Api *ApiSession) Owner() (common.Address, error) {
	return _Api.Contract.Owner(&_Api.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Api *ApiCallerSession) Owner() (common.Address, error) {
	return _Api.Contract.Owner(&_Api.CallOpts)
}

// DonateTofaucet is a paid mutator transaction binding the contract method 0x605e73cd.
//
// Solidity: function donateTofaucet() payable returns()
func (_Api *ApiTransactor) DonateTofaucet(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Api.contract.Transact(opts, "donateTofaucet")
}

// DonateTofaucet is a paid mutator transaction binding the contract method 0x605e73cd.
//
// Solidity: function donateTofaucet() payable returns()
func (_Api *ApiSession) DonateTofaucet() (*types.Transaction, error) {
	return _Api.Contract.DonateTofaucet(&_Api.TransactOpts)
}

// DonateTofaucet is a paid mutator transaction binding the contract method 0x605e73cd.
//
// Solidity: function donateTofaucet() payable returns()
func (_Api *ApiTransactorSession) DonateTofaucet() (*types.Transaction, error) {
	return _Api.Contract.DonateTofaucet(&_Api.TransactOpts)
}

// RequestTokens is a paid mutator transaction binding the contract method 0x580f3904.
//
// Solidity: function requestTokens(address _requestor) payable returns()
func (_Api *ApiTransactor) RequestTokens(opts *bind.TransactOpts, _requestor common.Address) (*types.Transaction, error) {
	return _Api.contract.Transact(opts, "requestTokens", _requestor)
}

// RequestTokens is a paid mutator transaction binding the contract method 0x580f3904.
//
// Solidity: function requestTokens(address _requestor) payable returns()
func (_Api *ApiSession) RequestTokens(_requestor common.Address) (*types.Transaction, error) {
	return _Api.Contract.RequestTokens(&_Api.TransactOpts, _requestor)
}

// RequestTokens is a paid mutator transaction binding the contract method 0x580f3904.
//
// Solidity: function requestTokens(address _requestor) payable returns()
func (_Api *ApiTransactorSession) RequestTokens(_requestor common.Address) (*types.Transaction, error) {
	return _Api.Contract.RequestTokens(&_Api.TransactOpts, _requestor)
}

// SetAmountallowed is a paid mutator transaction binding the contract method 0xf3aa52a1.
//
// Solidity: function setAmountallowed(uint256 newAmountAllowed) returns()
func (_Api *ApiTransactor) SetAmountallowed(opts *bind.TransactOpts, newAmountAllowed *big.Int) (*types.Transaction, error) {
	return _Api.contract.Transact(opts, "setAmountallowed", newAmountAllowed)
}

// SetAmountallowed is a paid mutator transaction binding the contract method 0xf3aa52a1.
//
// Solidity: function setAmountallowed(uint256 newAmountAllowed) returns()
func (_Api *ApiSession) SetAmountallowed(newAmountAllowed *big.Int) (*types.Transaction, error) {
	return _Api.Contract.SetAmountallowed(&_Api.TransactOpts, newAmountAllowed)
}

// SetAmountallowed is a paid mutator transaction binding the contract method 0xf3aa52a1.
//
// Solidity: function setAmountallowed(uint256 newAmountAllowed) returns()
func (_Api *ApiTransactorSession) SetAmountallowed(newAmountAllowed *big.Int) (*types.Transaction, error) {
	return _Api.Contract.SetAmountallowed(&_Api.TransactOpts, newAmountAllowed)
}

// SetOwner is a paid mutator transaction binding the contract method 0x13af4035.
//
// Solidity: function setOwner(address newOwner) returns()
func (_Api *ApiTransactor) SetOwner(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Api.contract.Transact(opts, "setOwner", newOwner)
}

// SetOwner is a paid mutator transaction binding the contract method 0x13af4035.
//
// Solidity: function setOwner(address newOwner) returns()
func (_Api *ApiSession) SetOwner(newOwner common.Address) (*types.Transaction, error) {
	return _Api.Contract.SetOwner(&_Api.TransactOpts, newOwner)
}

// SetOwner is a paid mutator transaction binding the contract method 0x13af4035.
//
// Solidity: function setOwner(address newOwner) returns()
func (_Api *ApiTransactorSession) SetOwner(newOwner common.Address) (*types.Transaction, error) {
	return _Api.Contract.SetOwner(&_Api.TransactOpts, newOwner)
}
