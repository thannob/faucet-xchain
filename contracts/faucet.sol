// SPDX-License-Identifier: UNLINCENSED
pragma solidity ^0.8.15;

contract faucet {
    address public owner;
    uint256 public amountAllowed = 100000000000000000;
    mapping(address => uint256) public lockTime;

    constructor() payable {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner can call this function.");
        _;
    }

    function setOwner(address newOwner) public onlyOwner {
        owner = newOwner;
    }

    function setAmountallowed(uint256 newAmountAllowed) public onlyOwner {
        amountAllowed = newAmountAllowed;
    }

    function donateTofaucet() public payable {}

    function requestTokens(address payable _requestor) public payable {
        require(
            block.timestamp > lockTime[_requestor],
            "Lock time (1 day) has not expired. Please try again later"
        );
        require(
            address(this).balance > amountAllowed,
            "Not enough funds in the faucet. Please donate"
        );
        require(
            _requestor.balance < amountAllowed,
            "You already have enough balance"
        );
        (bool success, ) = _requestor.call{value: amountAllowed}("");
        require(success);
        lockTime[_requestor] = block.timestamp + 1 days;
    }
}
